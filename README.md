# Onegeo-Suite Site Admin

## TABLE DES MATIÈRES

- [Pré-requis](#Pré-requis)
- [Installation](#Installation)
  - [Préparation des variables d'environnement](#Préparation-des-variables-d'environnement)
  - [Compilation des sources avec NPM](#Compilation-des-sources-avec-NPM)
- [Utilisation](#Utilisation)
- [Versions](#Version)
- [Auteur](#Auteur)

---

## Pré-requis

- nodejs (10.x)
- npm (6.x)

## Installation

### Installation des dépendances

### Dépendances spécifiques de développement

```console
npm install --dev-only
```

### Dépendances nécessaires

```console
npm install
```

### Variables d'environnement

```ini
NODE_ENV=development

DOMAIN=http://127.0.0.1:80
ADMIN_DOMAIN=https://127.0.0.1:8000
VUE_APP_DOMAIN=${DOMAIN}

BASE_PATH=/onegeo-publish
VUE_APP_BASE_PATH=${BASE_PATH}

VUE_APP_LOCALE=fr-FR

# App title
VUE_APP_TITLE=Onegeo-Suite

# Logo
VUE_APP_LOGO=@/assets/logo.png

# Favicon
VUE_APP_FAVICON_URL=${DOMAIN}/assets/favicon.png

# Client name
VUE_APP_CLIENT_NAME=ONEGEOSUITE

# Variables
VUE_APP_TABLE_PAGE_ITEMS_NUMBER=25
VUE_APP_RELOAD_INTERVAL=15000
VUE_APP_LABEL_ID_COMP=ID complémentaire

# API
VUE_APP_USERGROUP_API_PATH=/fr/usergroup/
VUE_APP_ORGANISATION_API_PATH=/fr/organisation/
VUE_APP_DATASET_API_PATH=/fr/dataset/
VUE_APP_RESOURCE_API_PATH=/fr/resource/
VUE_APP_USER_LOGIN_API_PATH=/fr/login/session/
VUE_APP_LOGIN_API_PATH=/fr/login/
VUE_APP_DASHBOARD_API_PATH=/fr/dashboard/

# Routes
VUE_APP_HOME_ROUTE=${DOMAIN}
VUE_APP_MAPS_ROUTE=${DOMAIN}/onegeo-maps/#/maps/
VUE_APP_PORTAIL_ROUTE=${DOMAIN}/portail/fr/
VUE_APP_STATS_ROUTE=${ADMIN_DOMAIN}/grafana/
VUE_APP_GEOSERVER_ROUTE=${DOMAIN}/geoserver
VUE_APP_GEONETWORK_ROUTE=${DOMAIN}/geonetwork
VUE_APP_LOGIN_ROUTE=${DOMAIN}/onegeo-login/
VUE_APP_LOGIN_PROFILE_ROUTE=${DOMAIN}/onegeo-login/profile/
VUE_APP_LOGIN_TERMS_ROUTE=${DOMAIN}/onegeo-login/terms-of-use/

# Force user
VUE_APP_API_ADMIN_USERNAME=admin
VUE_APP_API_ADMIN_PASSWORD=CHANGE_ME
```

### Compilation des sources avec NPM

```console
> npm run build
```

Les sources sont compilées dans un répertoire `dist` à la racine du projet.

## Utilisation

### Environnement de développement

```console
> npm run serve
```

### Environnement de production

Déployer le repertoire `dist` sur un serveur Web.

## Versions

1.0.0dev

## Auteur

2020-2021 Neogeo-Technologies
