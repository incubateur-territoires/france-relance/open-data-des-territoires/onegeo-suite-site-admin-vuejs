import axios from 'axios';

const DEV_AUTH = process.env.NODE_ENV === 'development' ? true : false;

const AUTH = {
  username: process.env.VUE_APP_API_ADMIN_USERNAME,
  password: process.env.VUE_APP_API_ADMIN_PASSWORD
};

const path = require('path');
const DOMAIN = process.env.VUE_APP_DOMAIN;
const DASHBOARD_API_PATH = process.env.VUE_APP_DASHBOARD_API_PATH;

if (!DEV_AUTH) {
  axios.defaults.headers.common['X-CSRFToken'] = (name => {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
  })('csrftoken');
}

const dashboardAPI = {

  async getTaskQueue(page = 1) {
    const url = new URL(path.join(DASHBOARD_API_PATH, `/task-queue-track/?page=${page}`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async orderTaskQueue(direction, field, page = 1) {
    const url = new URL(path.join(DASHBOARD_API_PATH, `task-queue-track/?ordering=${direction}${field}&page=${page}`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getTask(id) {
    const url = new URL(path.join(DASHBOARD_API_PATH, `task-queue-track/${id}/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  }

};

export default dashboardAPI;
