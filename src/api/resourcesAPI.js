import axios from 'axios';

const DEV_AUTH = process.env.NODE_ENV === 'development' ? true : false;

const AUTH = {
  username: process.env.VUE_APP_API_ADMIN_USERNAME,
  password: process.env.VUE_APP_API_ADMIN_PASSWORD
};

const path = require('path');
const DOMAIN = process.env.VUE_APP_DOMAIN;
const RESOURCE_API_PATH = process.env.VUE_APP_RESOURCE_API_PATH;
const DATASET_API_PATH = process.env.VUE_APP_DATASET_API_PATH;

if (!DEV_AUTH) {
  axios.defaults.headers.common['X-CSRFToken'] = (name => {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
  })('csrftoken');
}

const resourcesAPI = {

  async getResourcesList(filters, page = 1) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resources/?page=${page}`), DOMAIN);
    let filteredUrl;
    for (const filter in filters) {
      filteredUrl = url.href.concat('', `&${filter}=${filters[filter].join(',')}`);
    }
    const response = await axios.get(
      filteredUrl ? filteredUrl : url,
      { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getResource(id) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resources/${id}/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async patchResource(id, data, datasetId) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resources/${id}/`), DOMAIN);
    const response = await axios.patch(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      const url2 = new URL(path.join(DATASET_API_PATH, `datasets/${datasetId}/?action=update_or_create_and_push_record&asynchrone=false`), DOMAIN);
      const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
      const url3 = new URL(path.join(DATASET_API_PATH, `datasets/${datasetId}/?action=build_elastic_index&asynchrone=true`), DOMAIN);
      const response3 = await axios.patch(url3, {}, { ...DEV_AUTH && { auth: AUTH } });
      return response.data;
    }
    return false;
  },

  async deleteResourceToDataset(id) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resource-dataset/${id}/`), DOMAIN);
    const response = await axios.delete(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 204) {
      return response.data;
    }
    return false;
  },

  async deleteResource(id) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resources/${id}/`), DOMAIN);
    const response = await axios.delete(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 204) {
      return response.data;
    }
    return false;
  },

  async updateResource(id, data) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resources/${id}/`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getResourceDataFormats() {
    const url = new URL(path.join(RESOURCE_API_PATH, `data-format/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getResourceDataSource(location) {
    const url = new URL(location, DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getFTPChoices() {
    const url = new URL(path.join(RESOURCE_API_PATH, 'ftp-choices/'), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getResourceKinds() {
    const url = new URL(path.join(RESOURCE_API_PATH, `kinds/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },


  // File Upload requests

  async createFileUploadFormat(format) {
    const url = new URL(path.join(RESOURCE_API_PATH, `file-upload/`), DOMAIN);
    const response = await axios.post(url, { data_format: format }, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 201) {
      return response.data;
    }
    return false;
  },

  async createFileUploadFile(id, file) {
    const url = new URL(path.join(RESOURCE_API_PATH, `file-upload/${id}/`), DOMAIN);
    const response = await axios.put(url, file, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateFileUploadFormat(location, format) {
    const url = new URL(location, DOMAIN);
    const response = await axios.patch(url, { data_format: format }, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateFileUploadFile(location, file) {
    const url = new URL(location, DOMAIN);
    const response = await axios.put(url, file, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async deleteFileUploadFile(id) {
    const url = new URL(path.join(RESOURCE_API_PATH, `file-upload/${id}/`), DOMAIN);
    const response = await axios.delete(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 204) {
      return response.data;
    }
    return false;
  },

  async createFileUploadInspect(id) {
    const url = new URL(path.join(RESOURCE_API_PATH, `file-upload/${id}/?action=inspect&asynchrone=false`), DOMAIN);
    const response = await axios.patch(url, {}, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async createFileUploadResource(id, data, async = true) {
    const url = new URL(path.join(RESOURCE_API_PATH, `file-upload/${id}/?action=automatic_creation&asynchrone=${async}`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateFileUploadResource(id, data, datasetId, async = true) {
    const url = new URL(path.join(RESOURCE_API_PATH, `file-upload/${id}/?action=automatic_update&asynchrone=${async}`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },


  // FTP requests

  async createFTPFormat(data) {
    const url = new URL(path.join(RESOURCE_API_PATH, `ftp/`), DOMAIN);
    const response = await axios.post(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 201) {
      return response.data;
    }
    return false;
  },

  async createFTPInspect(id) {
    const url = new URL(path.join(RESOURCE_API_PATH, `ftp/${id}/?action=inspect&asynchrone=false`), DOMAIN);
    const response = await axios.patch(url, {}, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async createFTPResource(id, data, async = true) {
    const url = new URL(path.join(RESOURCE_API_PATH, `ftp/${id}/?action=automatic_creation&asynchrone=${async}`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateFTPResource(data, datasetId, async = true) {
    const url = new URL(path.join(RESOURCE_API_PATH, `ftp/${data.id}/`), DOMAIN);
    const response = await axios.patch(url, { href: data.href }, { ...DEV_AUTH && { auth: AUTH } });
    const url1 = new URL(path.join(RESOURCE_API_PATH, `ftp/${data.id}/?action=inspect&asynchrone=false`), DOMAIN);
    const response1 = await axios.patch(url1, {}, { ...DEV_AUTH && { auth: AUTH } });
    const url2 = new URL(path.join(RESOURCE_API_PATH, `ftp/${data.id}/?action=automatic_update&asynchrone=${async}`), DOMAIN);
    const response2 = await axios.patch(url2, {kwargs: data.kwargs}, { ...DEV_AUTH && { auth: AUTH } });
    if (response2.status === 200) {
      return response.data;
    }
    return false;
  },


  // HREF requests

  async createHrefFormat(data) {
    const url = new URL(path.join(RESOURCE_API_PATH, `href/`), DOMAIN);
    const response = await axios.post(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 201) {
      return response.data;
    }
    return false;
  },

  async createHrefInspect(id) {
    const url = new URL(path.join(RESOURCE_API_PATH, `href/${id}/?action=inspect&asynchrone=false`), DOMAIN);
    const response = await axios.patch(url, {}, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async createHrefResource(id, data, async = false) {
    const url = new URL(path.join(RESOURCE_API_PATH, `href/${id}/?action=automatic_creation&asynchrone=${async}`), DOMAIN);
    const response = await axios.patch(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateHrefResource(data, async = false) {
    const url = new URL(path.join(RESOURCE_API_PATH, `href/${data.id}/`), DOMAIN);
    const response = await axios.patch(url, { href: data.href }, { ...DEV_AUTH && { auth: AUTH } });
    const url1 = new URL(path.join(RESOURCE_API_PATH, `href/${data.id}/?action=inspect&asynchrone=false`), DOMAIN);
    const response1 = await axios.patch(url1, {}, { ...DEV_AUTH && { auth: AUTH } });
    const url2 = new URL(path.join(RESOURCE_API_PATH, `href/${data.id}/?action=automatic_update&asynchrone=${async}`), DOMAIN);
    const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
    if (response2.status === 200) {
      return response.data;
    }
    return false;
  },


  // Annexes
  
  async createFileUploadResourceAnnexe(id, data) {
    const url = new URL(path.join(RESOURCE_API_PATH, `file-upload/${id}/?action=create_resource&asynchrone=false`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateFileUploadAnnexe(datasetId, resourceId) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resources/${resourceId}/`), DOMAIN);
    const response = await axios.patch(url, {}, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      const url2 = new URL(path.join(DATASET_API_PATH, `datasets/${datasetId}/?action=build_elastic_index&asynchrone=true`), DOMAIN);
      const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
      return response.data;
    }
    return false;
  },

  async createFTPResourceAnnexe(id, data) {
    const url = new URL(path.join(RESOURCE_API_PATH, `ftp/${id}/?action=create_resource&asynchrone=false`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateFTPAnnexe(datasetId, resourceId) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resources/${resourceId}/`), DOMAIN);
    const response = await axios.patch(url, {}, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      const url2 = new URL(path.join(DATASET_API_PATH, `datasets/${datasetId}/?action=build_elastic_index&asynchrone=true`), DOMAIN);
      const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
      return response.data;
    }
    return false;
  },

  async createHrefResourceAnnexe(id, data) {
    const url = new URL(path.join(RESOURCE_API_PATH, `href/${id}/?action=create_resource&asynchrone=false`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async updateHrefAnnexe(data, datasetId, resourceId, async = false) {
    const url = new URL(path.join(RESOURCE_API_PATH, `href/${data.id}/`), DOMAIN);
    const response = await axios.patch(url, { href: data.href }, { ...DEV_AUTH && { auth: AUTH } });
    const url1 = new URL(path.join(RESOURCE_API_PATH, `href/${data.id}/?action=inspect&asynchrone=false`), DOMAIN);
    const response1 = await axios.patch(url1, {}, { ...DEV_AUTH && { auth: AUTH } });
    const url2 = new URL(path.join(RESOURCE_API_PATH, `resources/${resourceId}/`), DOMAIN);
    const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
    if (response2.status === 200) {
      const url3 = new URL(path.join(DATASET_API_PATH, `datasets/${datasetId}/?action=build_elastic_index&asynchrone=true`), DOMAIN);
      const response3 = await axios.patch(url3, {}, { ...DEV_AUTH && { auth: AUTH } });
      return response.data;
    }
    return false;
  },

  async appendAnnexeResourceToDataset(data) {
    const url = new URL(path.join(RESOURCE_API_PATH, `resource-dataset/`), DOMAIN);
    const response = await axios.post(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 201) {
      const url2 = new URL(path.join(DATASET_API_PATH, `datasets/${data.dataset}/?action=update_or_create_and_push_record&asynchrone=false`), DOMAIN);
      const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
      const url3 = new URL(path.join(DATASET_API_PATH, `datasets/${data.dataset}/?action=build_elastic_index&asynchrone=true`), DOMAIN);
      const response3 = await axios.patch(url3, {}, { ...DEV_AUTH && { auth: AUTH } });
    }
    return false;
  },

};

export default resourcesAPI;
