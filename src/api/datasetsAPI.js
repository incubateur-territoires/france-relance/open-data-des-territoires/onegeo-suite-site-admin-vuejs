import axios from 'axios';

const DEV_AUTH = process.env.NODE_ENV === 'development' ? true : false;

const AUTH = {
  username: process.env.VUE_APP_API_ADMIN_USERNAME,
  password: process.env.VUE_APP_API_ADMIN_PASSWORD
};

const path = require('path');
const DOMAIN = process.env.VUE_APP_DOMAIN;
const DATASET_API_PATH = process.env.VUE_APP_DATASET_API_PATH;

if (!DEV_AUTH) {
  axios.defaults.headers.common['X-CSRFToken'] = (name => {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
  })('csrftoken');
}

const datasetsAPI = {

  async getDatasetsCount() {
    const url = new URL(path.join(DATASET_API_PATH, `datasets-count/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetsList(page = 1) {
    const url = new URL(path.join(DATASET_API_PATH, `datasets/?page=${page}`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async orderDatasetsList(direction, field, page = 1) {
    const url = new URL(path.join(DATASET_API_PATH, `datasets/?ordering=${direction}${field}&page=${page}`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDataset(id) {
    const url = new URL(path.join(DATASET_API_PATH, `datasets/${id}/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetPermissions(id, filters, page = 1) {
    const url = new URL(path.join(DATASET_API_PATH, `usergroup-permissions/?dataset_id=${id}&page=${page}`), DOMAIN);
    let filteredUrl;
    if (Object.values(filters).some(el => el && el.length > 0)) {
      filteredUrl = url.href;
      for (const filter in filters) {
        if (filters[filter] && filters[filter].length > 0) {
          filteredUrl = filteredUrl.concat('', `&${filter}=${filters[filter].join(',')}`);
        }
      }
    }
    const response = await axios.get(
      filteredUrl ? filteredUrl : url,
      { ...DEV_AUTH && { auth: AUTH }
    });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async orderDatasetPermissions(direction, field, id, filters, page = 1) {
    const url = new URL(path.join(DATASET_API_PATH, `usergroup-permissions/?ordering=${direction}${field}&dataset_id=${id}&page=${page}`), DOMAIN);
    let filteredUrl;
    if (Object.values(filters).some(el => el && el.length > 0)) {
      filteredUrl = url.href;
      for (const filter in filters) {
        if (filters[filter] && filters[filter].length > 0) {
          filteredUrl = filteredUrl.concat('', `&${filter}=${filters[filter].join(',')}`);
        }
      }
    }
    const response = await axios.get(
      filteredUrl ? filteredUrl : url,
      { ...DEV_AUTH && { auth: AUTH }
    });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async setDatasetPermissions(level, data) {
    const url = new URL(path.join(DATASET_API_PATH, `usergroup-permission-actions/?action=set_permission_level_${level}`), DOMAIN);
    const response = await axios.put(
      url,
      data,
      { ...DEV_AUTH && { auth: AUTH }
    });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async createDataset(data) {
    const url = new URL(path.join(DATASET_API_PATH, `datasets/`), DOMAIN);
    const response = await axios.post(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 201) {
      return response.data;
    }
    return false;
  },

  async updateDataset(id, data) {
    const url = new URL(path.join(DATASET_API_PATH, `datasets/${id}/`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      const url2 = new URL(path.join(DATASET_API_PATH, `datasets/${id}/?action=update_or_create_and_push_record&asynchrone=false`), DOMAIN);
      const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
      const url3 = new URL(path.join(DATASET_API_PATH, `datasets/${id}/?action=build_elastic_index&asynchrone=true`), DOMAIN);
      const response3 = await axios.patch(url3, {}, { ...DEV_AUTH && { auth: AUTH } });
      return response.data;
    }
    return false;
  },

  async buildDatasetIndex(id) {
    try {
      const url2 = new URL(path.join(DATASET_API_PATH, `datasets/${id}/?action=update_or_create_and_push_record&asynchrone=false`), DOMAIN);
      const response2 = await axios.patch(url2, {}, { ...DEV_AUTH && { auth: AUTH } });
      const url3 = new URL(path.join(DATASET_API_PATH, `datasets/${id}/?action=build_elastic_index&asynchrone=true`), DOMAIN);
      const response3 = await axios.patch(url3, {}, { ...DEV_AUTH && { auth: AUTH } });
      return true;
    } catch (err) {
      console.error(err);
    }
  },

  async patchDataset(id, data) {
    const url = new URL(path.join(DATASET_API_PATH, `datasets/${id}/`), DOMAIN);
    const response = await axios.patch(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async setDatasetThumbnail(id, data) {
    const url =  new URL(path.join(DATASET_API_PATH, `datasets/${id}/thumbnail/`), DOMAIN);
    const response = await axios.put(url, data, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async deleteDataset(id) {
    const url =  new URL(path.join(DATASET_API_PATH, `datasets/${id}/`), DOMAIN);
    const response = await axios.delete(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 204) {
      return true;
    }
    return false;
  },

  async getDatasetsCategories() {
    const url =  new URL(path.join(DATASET_API_PATH, `categories/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetsGranularities() {
    const url =  new URL(path.join(DATASET_API_PATH, `granularities/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetsLicences() {
    const url =  new URL(path.join(DATASET_API_PATH, `licenses/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetsTopics() {
    const url =  new URL(path.join(DATASET_API_PATH, `topics/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetsKinds() {
    const url = new URL(path.join(DATASET_API_PATH, `kinds/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetsRestrictions() {
    const url = new URL(path.join(DATASET_API_PATH, `restrictions/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  },

  async getDatasetsUpdateFrequencies() {
    const url = new URL(path.join(DATASET_API_PATH, `update-frequencies/`), DOMAIN);
    const response = await axios.get(url, { ...DEV_AUTH && { auth: AUTH } });
    if (response.status === 200) {
      return response.data;
    }
    return false;
  }

};

export default datasetsAPI;
