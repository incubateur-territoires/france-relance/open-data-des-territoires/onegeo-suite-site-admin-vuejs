import Vue from 'vue';

// Bootstrap installation
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';

// Multiselect installation
import 'vue-multiselect/dist/vue-multiselect.min.css';

// Custom css
import '@/styles/app.scss';
import '@/styles/app.less';

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

import App from '@/App.vue';
import router from '@/router';
import store from '@/store';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
