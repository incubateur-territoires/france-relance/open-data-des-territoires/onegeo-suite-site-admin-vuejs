import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  // Gestion des comptes
  {
    path: '/accounts',
    name: 'Accounts',
    component: () => import('@/views/Accounts.vue'),
  },
  // Utilisateurs
  {
    path: '/accounts/users',
    name: 'Users',
    component: () => import('@/views/Users/Users.vue'),
  },
  {
    path: '/accounts/users/new',
    name: 'AddUser',
    component: () => import('@/views/Users/AddUser.vue'),
  },
  {
    path: '/accounts/users/:id',
    name: 'UserDetail',
    component: () => import('@/views/Users/UserDetail.vue'),
  },
  // Organisations
  {
    path: '/accounts/organisations',
    name: 'Organisations',
    component: () => import('@/views/Organisations/Organisations.vue'),
  },
  {
    path: '/accounts/organisations/new',
    name: 'AddOrganisation',
    component: () => import('@/views/Organisations/AddOrganisation.vue'),
  },
  {
    path: '/accounts/organisations/:id',
    name: 'OrganisationDetail',
    component: () => import('@/views/Organisations/OrganisationDetail.vue'),
  },
  // Groupes d'utilisateurs
  {
    path: '/accounts/groups',
    name: 'Groups',
    component: () => import('@/views/Groups/Groups.vue'),
  },
  {
    path: '/accounts/groups/new',
    name: 'AddGroup',
    component: () => import('@/views/Groups/AddGroup.vue'),
  },
  {
    path: '/accounts/groups/:id',
    name: 'GroupDetail',
    component: () => import('@/views/Groups/GroupDetail.vue'),
  },
  // Groupes d'organisations
  {
    path: '/accounts/spheres',
    name: 'Spheres',
    component: () => import('@/views/Spheres/Spheres.vue'),
  },
  {
    path: '/accounts/spheres/new',
    name: 'AddSphere',
    component: () => import('@/views/Spheres/AddSphere.vue'),
  },
  {
    path: '/accounts/spheres/:id',
    name: 'SphereDetail',
    component: () => import('@/views/Spheres/SphereDetail.vue'),
  },
  // Données
  {
    path: '/data',
    name: 'Data',
    component: () => import('@/views/Data.vue'),
  },
  // Jeux de données
  {
    path: '/data/datasets',
    name: 'Datasets',
    component: () => import('@/views/Datasets/Datasets.vue'),
  },
  {
    path: '/data/datasets/new',
    name: 'AddDataset',
    component: () => import('@/views/Datasets/AddDataset.vue'),
  },
  {
    path: '/data/datasets/:id',
    name: 'DatasetDetail',
    component: () => import('@/views/Datasets/DatasetDetail.vue'),
  },
  {
    path: '/data/datasets/:id/permissions',
    name: 'DatasetPermissions',
    component: () => import('@/views/Datasets/DatasetPermissions.vue'),
    props: true
  },
  // Ressources
  {
    path: '/data/resources',
    name: 'Resources',
    component: () => import('@/views/Resources/Resources.vue'),
  },
  {
    path: '/data/resources/new',
    name: 'AddResource',
    component: () => import('@/views/Resources/AddResource.vue'),
  },
  {
    path: '/data/resources/:id',
    name: 'ResourceDetail',
    component: () => import('@/views/Resources/ResourceDetail.vue'),
  },
  // Tasks
  {
    path: '/data/tasks',
    name: 'Tasks',
    component: () => import('@/views/Tasks/Tasks.vue'),
  },
  {
    path: '/data/tasks/:id',
    name: 'TaskDetail',
    component: () => import('@/views/Tasks/TaskDetail.vue'),
  },
  // Others
  {
    path: '/access-denied/',
    name: '403Page',
    component: () => import('@/views/403Page.vue'),
  },
  {
    path: '/*',
    name: 'NotFound',
    component: () => import('@/views/NotFound.vue'),
  }
];

const router = new VueRouter({
  routes,
  mode: 'history',
  base: process.env.VUE_APP_BASE_PATH || '',
});

router.beforeEach((to, from, next) => {
  if (store.state.reloadIntervalId) {
    store.commit('CLEAR_RELOAD_INTERVAL_ID');
  }
  store.dispatch('user-login/GET_USER_AUTHENTICATION')
  .then(() => {
    const user = store.state['user-login'].userData;
    if (to.path.includes('account')) {
      if (user.is_superuser || (user.usergroup_roles && user.usergroup_roles.some(el => el.role === 2))) {
        next();
      } else {
        next({ name: '403Page' });
      }
    } else if (to.path.includes('data')) {
      if (user.is_superuser || (user.usergroup_roles && user.usergroup_roles.some(el => el.role >= 1))) {
        next();
      } else {
        next({ name: '403Page' });
      }
    } else {
      next();
    }
  });
});

export default router;
