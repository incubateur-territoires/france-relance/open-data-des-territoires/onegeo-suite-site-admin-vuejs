import Vue from 'vue';
import Vuex from 'vuex';
import modules from './modules';

Vue.use(Vuex);

const state = {
  // config: null,
  cancellableSearchRequest: [],
  reloadIntervalId: null
};

// export const SET_CONFIG = 'SET_CONFIG';
export const SET_CANCELLABLE_SEARCH_REQUEST = 'SET_CANCELLABLE_SEARCH_REQUEST';
export const RESET_CANCELLABLE_SEARCH_REQUEST = 'RESET_CANCELLABLE_SEARCH_REQUEST';
export const SET_RELOAD_INTERVAL_ID = 'SET_RELOAD_INTERVAL_ID';
export const CLEAR_RELOAD_INTERVAL_ID = 'CLEAR_RELOAD_INTERVAL_ID';

const mutations = {
  // [SET_CONFIG]: (state, payload) => {
  //   state.config = payload;
  // },

  [SET_CANCELLABLE_SEARCH_REQUEST]: (state, payload) => {
    state.cancellableSearchRequest.push(payload);
  },

  [RESET_CANCELLABLE_SEARCH_REQUEST]: (state) => {
    state.cancellableSearchRequest = [];
  },


  [SET_RELOAD_INTERVAL_ID]: (state, payload) => {
    state.reloadIntervalId = payload;
  },

  [CLEAR_RELOAD_INTERVAL_ID]: (state) => {
    clearInterval(state.reloadIntervalId);
    state.reloadIntervalId = null;
  }
};

export default new Vuex.Store({
  modules,
  state,
  mutations
});
