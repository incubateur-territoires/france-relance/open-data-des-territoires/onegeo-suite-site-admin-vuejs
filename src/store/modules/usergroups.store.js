import router from '@/router';
import usergroupsAPI from '@/api/usergroupsAPI.js';
import { ErrorService } from '@/services/error-service.js';

import axios from 'axios';
const DEV_AUTH = process.env.NODE_ENV === 'development' ? true : false;
const AUTH = {
  username: process.env.VUE_APP_API_ADMIN_USERNAME,
  password: process.env.VUE_APP_API_ADMIN_PASSWORD
};
const path = require('path');
const DOMAIN = process.env.VUE_APP_DOMAIN;
const USERGROUP_API_PATH = process.env.VUE_APP_USERGROUP_API_PATH;

const USERGROUP_TYPES = {
  organisation: 'organisation',
  group: 'user-group',
  sphere : 'group-of-organisation'
};

// MUTATIONS
export const SET_USERGROUPS_TYPES = 'SET_USERGROUPS_TYPES';
export const SET_USERGROUPS_LIST = 'SET_USERGROUPS_LIST';
export const SET_USERGROUPS_GROUPS_LIST = 'SET_USERGROUPS_GROUPS_LIST';
export const SET_USERGROUPS_SPHERES_LIST = 'SET_USERGROUPS_SPHERES_LIST';
export const SET_CURRENT_USERGROUP = 'SET_CURRENT_USERGROUP';
export const SET_CURRENT_USERGROUP_MEMBERS = 'SET_CURRENT_USERGROUP_MEMBERS';
export const SET_CURRENT_USERGROUP_CHILDREN = 'SET_CURRENT_USERGROUP_CHILDREN';
export const SET_USERGROUPS_ROLES = 'SET_USERGROUPS_ROLES';
export const SET_ERROR = 'SET_ERROR';
export const SET_IS_GROUPS_SEARCHED = 'SET_IS_GROUPS_SEARCHED';
export const SET_IS_SPHERES_SEARCHED = 'SET_IS_SPHERES_SEARCHED';
export const SET_USERGROUPS_FILTERS = 'SET_USERGROUPS_FILTERS';
export const REMOVE_USERGROUPS_FILTERS = 'REMOVE_USERGROUPS_FILTERS';
export const RESET_USERGROUPS_FILTERS = 'RESET_USERGROUPS_FILTERS';

// ACTIONS
export const CREATE_USERGROUP = 'CREATE_USERGROUP';
export const DELETE_USERGROUP = 'DELETE_USERGROUP';
export const GET_USERGROUPS_LIST = 'GET_USERGROUPS_LIST';
export const GET_USERGROUPS_TYPES = 'GET_USERGROUPS_TYPES';
export const GET_USERGROUPS_GROUPS_LIST = 'GET_USERGROUPS_GROUPS_LIST';
export const GET_USERGROUPS_SPHERES_LIST = 'GET_USERGROUPS_SPHERES_LIST';
export const GET_USERGROUP_DETAIL = 'GET_USERGROUP_DETAIL';
export const GET_USERGROUP_MEMBERS = 'GET_USERGROUP_MEMBERS';
export const GET_USERGROUP_CHILDREN = 'GET_USERGROUP_CHILDREN';
export const GET_USERGROUPS_ROLES = 'GET_USERGROUPS_ROLES';
export const UPDATE_USERGROUP = 'UPDATE_USERGROUP';
export const PATCH_USERGROUP = 'PATCH_USERGROUP';
export const SEARCH_USERGROUPS_LIST = 'SEARCH_USERGROUPS_LIST';
export const HANDLE_SEARCH_REQUEST = 'HANDLE_SEARCH_REQUEST';
export const SET_LAST_CREATED_USERGROUP = 'SET_LAST_CREATED_USERGROUP';

/**************** STATE *******************/
const state = {
  usergroupsTypes: [],
  usergroupsCount: 0,
  usergroupsList: [],
  spheresCount: 0,
  spheres: [],
  groupsCount: 0,
  groups: [],
  usergroupsRoles: [],
  currentUsergroup: null,
  currentUsergroupMembers: [],
  currentUsergroupMembersCount: null,
  currentUsergroupChildren: [],
  currentUsergroupChildrenCount: null,
  usergroupsError: null,
  lastCreatedUsergroup: null,
  isGroupsSearched: false,
  isSpheresSearched: false,
  currentUsergroupsFilters: {
    usergroup__status__in: [],
    user__role__in: []
  },
};

/**************** GETTERS *****************/
const getters = {
  tablePage: (state, getters, rootState) => {
    return rootState.pagination.currentPage;
  }
};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_USERGROUPS_TYPES]: (state, payload) => {
    state.usergroupsTypes = payload;
  },

  [SET_USERGROUPS_LIST]: (state, payload) => {
    if (payload) {
      state.usergroupsCount = payload.count;
      state.usergroupsList = payload.results;
    }
  },

  [SET_USERGROUPS_GROUPS_LIST]: (state, payload) => {
    if (payload) {
      state.groupsCount = payload.count;
      state.groups = payload.results;
    }
  },

  [SET_USERGROUPS_SPHERES_LIST]: (state, payload) => {
    if (payload) {
      state.spheresCount = payload.count;
      state.spheres = payload.results;
    }
  },

  [SET_CURRENT_USERGROUP]: (state, payload) => {
    state.currentUsergroup = payload;
  },

  [SET_CURRENT_USERGROUP_MEMBERS]: (state, payload) => {
    state.currentUsergroupMembersCount = payload.count;
    state.currentUsergroupMembers = payload.results;
  },

  [SET_CURRENT_USERGROUP_CHILDREN]: (state, payload) => {
    state.currentUsergroupChildrenCount = payload.count;
    state.currentUsergroupChildren = payload.results;
  },

  [SET_USERGROUPS_ROLES]: (state, payload) => {
    state.usergroupsRoles = payload;
  },

  [SET_ERROR]: (state, error) => {
    if (error) {
      ErrorService.onError(error);
      state.usergroupsError = error;
    } else {
      state.usergroupsError = error;
    }
  },

  [SET_LAST_CREATED_USERGROUP]: (state, payload) => {
    state.lastCreatedUsergroup = payload;
  },

  [SET_IS_GROUPS_SEARCHED]: (state, payload) => {
    state.isGroupsSearched = payload;
  },

  [SET_IS_SPHERES_SEARCHED]: (state, payload) => {
    state.isSpheresSearched = payload;
  },

  [SET_USERGROUPS_FILTERS]: (state, payload) => {
    const index = state.currentUsergroupsFilters[payload.filter].findIndex(el => el === payload.value);
    if (index === -1) {
      state.currentUsergroupsFilters[payload.filter].push(payload.value);
    }
  },

  [REMOVE_USERGROUPS_FILTERS]: (state, payload) => {
    state.currentUsergroupsFilters[payload.filter.filter].splice(payload.index, 1);
  },

  [RESET_USERGROUPS_FILTERS]: (state) => {
    state.currentUsergroupsFilters = {
      usergroup__status__in: []
    };
  },
};
/**************** ACTIONS *****************/
const actions = {
  [GET_USERGROUPS_TYPES]: async({ commit }) => {
    const usergroupTypes = await usergroupsAPI.getUsergroupsTypes();
    commit('SET_USERGROUPS_TYPES', usergroupTypes);
  },
  [GET_USERGROUPS_LIST]: async ({ state, commit, getters }, { direction, field, usergroupTypes, page }) => {
    if (!page) {
      page = getters.tablePage;
    }
    if (field) {
      await usergroupsAPI.orderUsergroupsList(direction, field, usergroupTypes, page, state.currentUsergroupsFilters)
        .then((usergroups) => {
          if (usergroups) {
            commit('SET_ERROR', null);
            commit('SET_USERGROUPS_LIST', usergroups);
          }
        })
        .catch((error) => {
          commit('SET_ERROR', error);
        });
    } else {
      await usergroupsAPI.getUsergroupsList(usergroupTypes, page, state.currentUsergroupsFilters)
        .then((usergroups) => {
          if (usergroups) {
            commit('SET_ERROR', null);
            commit('SET_USERGROUPS_LIST', usergroups);
          }
        })
        .catch((error) => {
          commit('SET_ERROR', error);
        });
    }
  },

  [GET_USERGROUPS_GROUPS_LIST]: async ({ state, commit, getters }, { direction, field, page }) => {
    if (!page) {
      page = getters.tablePage;
    }
    if (field) {
      await usergroupsAPI.orderFilteredUsergroupsList(direction, field, USERGROUP_TYPES.group, page, state.currentUsergroupsFilters)
        .then((groups) => {
          if (groups) {
            commit('SET_ERROR', null);
            commit('SET_USERGROUPS_GROUPS_LIST', groups);
            commit('SET_IS_GROUPS_SEARCHED', false);
          }
        })
        .catch((error) => {
          commit('SET_ERROR', error);
        });
    } else {
      await usergroupsAPI.getFilteredUsergroupsList(USERGROUP_TYPES.group, page, state.currentUsergroupsFilters)
        .then((groups) => {
          if (groups) {
            commit('SET_ERROR', null);
            commit('SET_USERGROUPS_GROUPS_LIST', groups);
            commit('SET_IS_GROUPS_SEARCHED', false);
          }
        })
        .catch((error) => {
          commit('SET_ERROR', error);
        });
    }
  },

  [GET_USERGROUPS_SPHERES_LIST]: async ({ state, commit, getters }, { direction, field, page }) => {
    if (!page) {
      page = getters.tablePage;
    }
    if (field) {
      await usergroupsAPI.orderFilteredUsergroupsList(direction, field, USERGROUP_TYPES.sphere, page, state.currentUsergroupsFilters)
        .then((spheres) => {
          if (spheres) {
            commit('SET_ERROR', null);
            commit('SET_USERGROUPS_SPHERES_LIST', spheres);
            commit('SET_IS_SPHERES_SEARCHED', false);
          }
        })
        .catch((error) => {
          commit('SET_ERROR', error);
        });
    } else {
      await usergroupsAPI.getFilteredUsergroupsList(USERGROUP_TYPES.sphere, page, state.currentUsergroupsFilters)
        .then((spheres) => {
          if (spheres) {
            commit('SET_ERROR', null);
            commit('SET_USERGROUPS_SPHERES_LIST', spheres);
            commit('SET_IS_SPHERES_SEARCHED', false);
          }
        })
        .catch((error) => {
          commit('SET_ERROR', error);
        });
    }
  },

  [SEARCH_USERGROUPS_LIST]: async ({ dispatch }, { text, types }) => {

    if (types.length === 0) {
      if (router.currentRoute.name === 'Groups') {
        types.push(USERGROUP_TYPES.group);
      } else if (router.currentRoute.name === 'Spheres') {
        types.push(USERGROUP_TYPES.sphere);
      }
    }

    if (text) {
      await dispatch('HANDLE_SEARCH_REQUEST', { text, types });
    } else {
      if (types.length === 1 && types[0] === USERGROUP_TYPES.sphere) {
        await dispatch('GET_USERGROUPS_SPHERES_LIST', {
          direction: null,
          field: null
        });
      } else if (types.length === 1 && types[0] === USERGROUP_TYPES.group) {
        await dispatch('GET_USERGROUPS_GROUPS_LIST', {
          direction: null,
          field: null
        });
      } else {
        await dispatch('GET_USERGROUPS_LIST', {
          direction: null,
          field: null,
          usergroupTypes: types
        });
      }
    }
  },

  [HANDLE_SEARCH_REQUEST]: async ({ state, rootState, getters, commit }, { text, types }) => {
    if (rootState.cancellableSearchRequest.length > 0) {
      const currentRequestCancelToken =
        rootState.cancellableSearchRequest[rootState.cancellableSearchRequest.length - 1];
      currentRequestCancelToken.cancel();
    }

    const cancelToken = axios.CancelToken.source();
    commit('SET_CANCELLABLE_SEARCH_REQUEST', cancelToken, { root: true });
    const url = new URL(path.join(USERGROUP_API_PATH, `user-groups/?page=${getters.tablePage}&search=${text}&usergroup_types=${types.join(',')}`), DOMAIN);
    let filteredUrl;
    for (const filter in state.currentUsergroupsFilters) {
      if (state.currentUsergroupsFilters[filter] && state.currentUsergroupsFilters[filter].length) {
        filteredUrl = url.href.concat('', `&${filter}=${state.currentUsergroupsFilters[filter].join(',')}`);
      }
    }

    try {
      const response = await axios.get(
        filteredUrl ? filteredUrl : url,
        {
          cancelToken: cancelToken.token,
          ...DEV_AUTH && { auth: AUTH }
        }
      );
      if (response.status === 200) {
        const usergroups = response.data;
        if (usergroups) {
          commit('SET_ERROR', null);
          if (types.length === 1 && types[0] === 'group-of-organisation') {
            commit('SET_USERGROUPS_SPHERES_LIST', usergroups);
            commit('SET_IS_SPHERES_SEARCHED', true);
          } else if (types.length === 1 && types[0] === USERGROUP_TYPES.group) {
            commit('SET_USERGROUPS_GROUPS_LIST', usergroups);
            commit('SET_IS_GROUPS_SEARCHED', true);
          } else {
            commit('SET_USERGROUPS_LIST', usergroups);
          }
        }
      }
    } catch(err) {
      commit('SET_ERROR', err);
    }
  },

  [GET_USERGROUP_DETAIL]: async ({ commit }, id) => {
    await usergroupsAPI.getUsergroup(id)
      .then((usergroup) => {
        if (usergroup) {
          commit('SET_ERROR', null);
          commit('SET_CURRENT_USERGROUP', usergroup);
        }
      })
      .catch((error) => {
        commit('SET_ERROR', error);
      });
  },

  [GET_USERGROUP_MEMBERS]: async ({ state, commit }, { id, direction, field }) => {
    if (!id) {
      id = state.currentUsergroup.id;
    }
    await usergroupsAPI.getUsergroupMembers(id, direction, field)
      .then((members) => {
        if (members) {
          commit('SET_ERROR', null);
          commit('SET_CURRENT_USERGROUP_MEMBERS', members);
        }
      })
      .catch((error) => {
        commit('SET_ERROR', error);
      });
  },

  [GET_USERGROUP_CHILDREN]: async ({ state, commit }, { id, direction, field }) => {
    if (!id) {
      id = state.currentUsergroup.id;
    }
    await usergroupsAPI.getUsergroupChildren(id, direction, field)
      .then((children) => {
        if (children) {
          commit('SET_ERROR', null);
          commit('SET_CURRENT_USERGROUP_CHILDREN', children);
        }
      })
      .catch((error) => {
        commit('SET_ERROR', error);
      });
  },

  [GET_USERGROUPS_ROLES]: async ({ commit }) => {
    await usergroupsAPI.getUsergroupRoles()
      .then((roles) => {
        if (roles) {
          commit('SET_ERROR', null);
          commit('SET_USERGROUPS_ROLES', roles);
        }
      })
      .catch((error) => {
        commit('SET_ERROR', error);
      });
  },

  [UPDATE_USERGROUP]: async ({ commit }, { id, data }) => {
    await usergroupsAPI.updateUsergroup(id, data)
      .then((usergroup) => {
        if (usergroup) {
          commit('SET_ERROR', null);
          ErrorService.onSuccess(usergroup, "Le groupe d'utilisateurs a été modifié avec succès.");
        }
      })
      .catch((error) => {
        commit('SET_ERROR', error);
      });
  },

  [PATCH_USERGROUP]: async ({ commit }, { id, data }) => {
    await usergroupsAPI.patchUsergroup(id, data).then((resp) => {
      if (resp) {
        commit('SET_ERROR', null);
        ErrorService.onSuccess(resp, "Le groupe d'utilisateurs a été modifié avec succès.");
      }
    }).catch((error) => {
      commit('SET_ERROR', error);
    });
  },

  [CREATE_USERGROUP]: async ({ commit }, data) => {
    await usergroupsAPI.createUsergroup(data)
      .then((usergroup) => {
        if (usergroup) {
          commit('SET_ERROR', null);
          ErrorService.onSuccess(usergroup, "Le groupe d'utilisateurs a été créé avec succès.");
          commit('SET_LAST_CREATED_USERGROUP', usergroup);
        }
      })
      .catch((error) => {
        commit('SET_ERROR', error);
      });
  },

  [DELETE_USERGROUP]: async ({ commit }, id) => {
    await usergroupsAPI.deleteUsergroup(id)
      .then(() => {
        commit('SET_ERROR', null);
        ErrorService.onSuccess(true, "Le groupe d'utilisateurs a été supprimé avec succès.");
      })
      .catch((error) => {
        commit('SET_ERROR', error);
      });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
