// MUTATIONS
export const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';

// ACTIONS

/**************** STATE *******************/
const state = {
  currentPage: 1
};

/**************** GETTERS *****************/
const getters = {};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_CURRENT_PAGE]: (state, payload) => {
    state.currentPage = payload;
  }
};
/**************** ACTIONS *****************/
const actions = {
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
