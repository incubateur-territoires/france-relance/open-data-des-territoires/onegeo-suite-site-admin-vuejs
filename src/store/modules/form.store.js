// MUTATIONS
export const SET_CURRENT_FORM = 'SET_CURRENT_FORM';

// ACTIONS

/**************** STATE *******************/
const state = {
  currentFormId: null,
  currentForm: {}
};

/**************** GETTERS *****************/
const getters = {};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_CURRENT_FORM]: (state, payload) => {
    state.currentFormId = payload.id;
    state.currentForm = payload.form;
  }
};
/**************** ACTIONS *****************/
const actions = {
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
