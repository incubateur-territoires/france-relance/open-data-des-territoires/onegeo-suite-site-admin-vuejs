import userLoginAPI from '@/api/userLoginAPI.js';

// MUTATIONS
export const SET_USER_AUTHENTICATION = 'SET_USER_AUTHENTICATION';

// ACTIONS
export const GET_USER_AUTHENTICATION = 'GET_USER_AUTHENTICATION';

/**************** STATE *******************/
const state = {
  isUserAuthenticated: false,
  userData: {}
};

/**************** GETTERS *****************/
const getters = {

};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_USER_AUTHENTICATION]: (state, payload) => {
    state.isUserAuthenticated = payload.authenticated ? payload.authenticated : false;
    if (payload.user) {
      state.userData = payload.user;
    }
  }
};
/**************** ACTIONS *****************/
const actions = {
  [GET_USER_AUTHENTICATION]: async ({ commit }) => {
    const authentication = await userLoginAPI.getUserLogin();
    commit('SET_USER_AUTHENTICATION', authentication);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
