import dashboardAPI from '@/api/dashboardAPI.js';

// MUTATIONS
export const SET_DASHBOARD_TASKS = 'SET_DASHBOARD_TASKS';
export const SET_CURRENT_TASK = 'SET_CURRENT_TASK';


// ACTIONS
export const GET_DASHBOARD_TASKS = 'GET_DASHBOARD_TASKS';
export const GET_TASK = 'GET_TASK';

/**************** STATE *******************/
const state = {
  dashboardTasksCount: null,
  dashboardTasksQueue: [],
  currentTask: null
};

/**************** GETTERS *****************/
const getters = {
  tablePage: (state, getters, rootState) => {
    return rootState.pagination.currentPage;
  }
};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_DASHBOARD_TASKS]: (state, payload) => {
    state.dashboardTasksQueue = payload.results;
    state.dashboardTasksCount = payload.count;
  },

  [SET_CURRENT_TASK]: (state, payload) => {
    state.currentTask = payload;
  }
};
/**************** ACTIONS *****************/
const actions = {
  [GET_DASHBOARD_TASKS]: async ({ commit, getters }, { direction, field, page }) => {
    let tasksQueue;
    if (!page) {
      page = getters.tablePage;
    }
    if (field) {
      tasksQueue = await dashboardAPI.orderTaskQueue(direction, field, page);
    } else {
      tasksQueue = await dashboardAPI.getTaskQueue(page);
    }
    commit('SET_DASHBOARD_TASKS', tasksQueue);
  },

  [GET_TASK]: async ({ commit }, { id }) => {
    const task = await dashboardAPI.getTask(id);
    commit('SET_CURRENT_TASK', task);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
