import datasetsAPI from '@/api/datasetsAPI.js';
import { ErrorService } from '@/services/error-service.js';
import axios from 'axios';

const DEV_AUTH = process.env.NODE_ENV === 'development' ? true : false;
const AUTH = {
  username: process.env.VUE_APP_API_ADMIN_USERNAME,
  password: process.env.VUE_APP_API_ADMIN_PASSWORD
};
const path = require('path');
const DOMAIN = process.env.VUE_APP_DOMAIN;
const DATASET_API_PATH = process.env.VUE_APP_DATASET_API_PATH;

// MUTATIONS
export const SET_DATASETS_COUNT = 'SET_DATASETS_COUNT';
export const SET_CURRENT_DATASET = 'SET_CURRENT_DATASET';
export const SET_CURRENT_USERGROUPS_PERMISSIONS = 'SET_CURRENT_USERGROUPS_PERMISSIONS';
export const SET_DATASETS_LIST = 'SET_DATASETS_LIST';
export const SET_DATASETS_CATEGORIES = 'SET_DATASETS_CATEGORIES';
export const SET_DATASETS_GRANULARITIES = 'SET_DATASETS_GRANULARITIES';
export const SET_DATASETS_LICENCES = 'SET_DATASETS_LICENCES';
export const SET_DATASETS_TOPICS = 'SET_DATASETS_TOPICS';
export const SET_DATASETS_KINDS = 'SET_DATASETS_KINDS';
export const SET_DATASETS_RESTRICTIONS = 'SET_DATASETS_RESTRICTIONS';
export const SET_DATASETS_UPDATE_FREQUENCIES = 'SET_DATASETS_UPDATE_FREQUENCIES';
export const SET_ERROR = 'SET_ERROR';
export const SET_LAST_CREATED_DATASET = 'SET_LAST_CREATED_DATASET';
export const SET_USERGROUPS_PERMISSIONS_FILTERS = 'SET_USERGROUPS_PERMISSIONS_FILTERS';
export const REMOVE_USERGROUPS_PERMISSIONS_FILTERS = 'REMOVE_USERGROUPS_PERMISSIONS_FILTERS';
export const RESET_USERGROUPS_PERMISSIONS_FILTERS = 'RESET_USERGROUPS_PERMISSIONS_FILTERS';
export const SET_IS_PERMISSIONS_LIST_SEARCHED = 'SET_IS_PERMISSIONS_LIST_SEARCHED';
export const SET_USERGROUPS_PERMISSIONS_ORDERING = 'SET_USERGROUPS_PERMISSIONS_ORDERING';
export const SET_IS_DATASETSLIST_SEARCHED = 'SET_IS_DATASETSLIST_SEARCHED';
export const SET_DATASETS_ORDERING = 'SET_DATASETS_ORDERING';
export const SET_SELECTED_PERMISSIONS = 'SET_SELECTED_PERMISSIONS';
export const RESET_SELECTED_PERMISSIONS = 'RESET_SELECTED_PERMISSIONS';

// ACTIONS
export const PRE_SET_USERGROUPS_PERMISSIONS_FILTERS = 'PRE_SET_USERGROUPS_PERMISSIONS_FILTERS';
export const PRE_REMOVE_USERGROUPS_PERMISSIONS_FILTERS = 'PRE_REMOVE_USERGROUPS_PERMISSIONS_FILTERS';
export const PRE_RESET_USERGROUPS_PERMISSIONS_FILTERS = 'PRE_RESET_USERGROUPS_PERMISSIONS_FILTERS';
export const GET_DATASET_DETAIL = 'GET_DATASET_DETAIL';
export const GET_DATASET_USERGROUPS_PERMISSIONS = 'GET_DATASET_USERGROUPS_PERMISSIONS';
export const SET_DATASET_USERGROUPS_PERMISSIONS = 'SET_DATASET_USERGROUPS_PERMISSIONS';
export const SEARCH_DATASET_USERGROUPS_PERMISSIONS = 'SEARCH_DATASET_USERGROUPS_PERMISSIONS';
export const HANDLE_PERMISSIONS_SEARCH_REQUEST = 'HANDLE_PERMISSIONS_SEARCH_REQUEST';
export const GET_DATASETS_COUNT = 'GET_DATASETS_COUNT';
export const GET_DATASETS_LIST = 'GET_DATASETS_LIST';
export const SEARCH_DATASETS_LIST = 'SEARCH_DATASETS_LIST';
export const GET_DATASETS_CATEGORIES = 'GET_DATASETS_CATEGORIES';
export const GET_DATASETS_GRANULARITIES = 'GET_DATASETS_GRANULARITIES';
export const GET_DATASETS_LICENCES = 'GET_DATASETS_LICENCES';
export const GET_DATASETS_TOPICS = 'GET_DATASETS_TOPICS';
export const GET_DATASETS_KINDS = 'GET_DATASETS_KINDS';
export const GET_DATASETS_RESTRICTIONS = 'GET_DATASETS_RESTRICTIONS';
export const GET_DATASETS_UPDATE_FREQUENCIES = 'GET_DATASETS_UPDATE_FREQUENCIES';
export const CREATE_DATASET = 'CREATE_DATASET';
export const UPDATE_DATASET = 'UPDATE_DATASET';
export const PATCH_DATASET = 'PATCH_DATASET';
export const DELETE_DATASET = 'DELETE_DATASET';
export const SET_DATASET_THUMBNAIL = 'SET_DATASET_THUMBNAIL';
export const HANDLE_DATASETS_SEARCH_REQUEST = 'HANDLE_DATASETS_SEARCH_REQUEST';

/**************** STATE *******************/
const state = {
  datasetsCount: 0,
  datasetsList: [],
  currentDataset: {},
  datasetsCategories: [],
  datasetsGranularities: [],
  datasetsLicences: [],
  datasetsTopics: [],
  datasetsKinds: [],
  datasetsRestrictions: [],
  datasetsUpdateFrequencies: [],
  datasetsError: null,
  lastCreatedDataset: null,
  duplicatedDataset: null,
  // permissions
  currentUsergroupsPermissionsCount: 0,
  currentUsergroupsPermissions: [],
  currentUsergroupsPermissionsFilters: {
    usergroup_type_id: [],
    organisation_type_id: [],
    level: []
  },
  // search and filter
  searchDatasetsFilter: null,
  isDatasetsListSearched: null,
  isPermissionsListSearched: false,
  searchPermissionsFilter: null,
  currentUsergroupsPermissionsOrdering: {
    direction: null,
    field: null
  },
  currentDatasetsOrdering: {
    direction: null,
    field: null
  },
  selectedPermissions: []
};

/**************** GETTERS *****************/
const getters = {
  tablePage: (state, getters, rootState) => {
    return rootState.pagination.currentPage;
  }
};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_DATASETS_COUNT]: (state, payload) => {
    state.datasetsCount = payload.count;
  },
  [SET_DATASETS_LIST]: (state, payload) => {
    if (payload && payload.results) {
      state.datasetsList = payload.results;
    }
    if (payload && payload.count !== null) {
      state.datasetsCount = payload.count;
    }
  },
  [SET_CURRENT_DATASET]: (state, payload) => {
    state.currentDataset = payload;
  },
  [SET_CURRENT_USERGROUPS_PERMISSIONS]: (state, payload) => {
    state.currentUsergroupsPermissionsCount = payload.count;
    state.currentUsergroupsPermissions = payload.results;
  },
  [SET_DATASETS_CATEGORIES]: (state, payload) => {
    state.datasetsCategories = payload;
  },
  [SET_DATASETS_GRANULARITIES]: (state, payload) => {
    state.datasetsGranularities = payload;
  },
  [SET_DATASETS_LICENCES]: (state, payload) => {
    state.datasetsLicences = payload;
  },
  [SET_DATASETS_TOPICS]: (state, payload) => {
    state.datasetsTopics = payload;
  },
  [SET_DATASETS_KINDS]: (state, payload) => {
    state.datasetsKinds = payload;
  },
  [SET_DATASETS_RESTRICTIONS]: (state, payload) => {
    state.datasetsRestrictions = payload;
  },
  [SET_DATASETS_UPDATE_FREQUENCIES]: (state, payload) => {
    state.datasetsUpdateFrequencies = payload;
  },
  [SET_ERROR]: (state, error) => {
    if (error) {
      ErrorService.onError(error);
      state.datasetsError = error;
    } else {
      state.datasetsError = error;
    }
  },
  [SET_LAST_CREATED_DATASET]: (state, payload) => {
    state.lastCreatedDataset = payload;
  },
  SET_DUPLICATED_DATASET: (state, payload) => {
    state.duplicatedDataset = payload;
  },
  [SET_USERGROUPS_PERMISSIONS_FILTERS]: (state, payload) => {
    if (payload.filter === 'level') {
      state.currentUsergroupsPermissionsFilters[payload.filter] = payload.value;
    } else if (state.currentUsergroupsPermissionsFilters[payload.filter].findIndex(el => el === payload.value) === -1) {
      if (payload.filter === 'organisation_type_id') {
        state.currentUsergroupsPermissionsFilters[payload.filter] = [payload.value];
      } else {
        state.currentUsergroupsPermissionsFilters[payload.filter].push(payload.value);
      }
    }
  },
  [REMOVE_USERGROUPS_PERMISSIONS_FILTERS]: (state, payload) => {
    state.currentUsergroupsPermissionsFilters[payload.filter.filter].splice(payload.index, 1);
  },
  [RESET_USERGROUPS_PERMISSIONS_FILTERS]: (state, payload) => {
    state.currentUsergroupsPermissionsFilters[payload] = [];
  },

  [SET_IS_PERMISSIONS_LIST_SEARCHED]: (state, payload) => {
    state.isPermissionsListSearched = payload.isSearched;
    state.searchPermissionsFilter = payload.text;
  },

  [SET_USERGROUPS_PERMISSIONS_ORDERING]: (state, payload) => {
    state.currentUsergroupsPermissionsOrdering = payload;
  },

  [SET_IS_DATASETSLIST_SEARCHED]: (state, payload) => {
    state.isDatasetsListSearched = payload.isSearched;
    state.searchDatasetsFilter = payload.text;
  },

  [SET_DATASETS_ORDERING]: (state, payload) => {
    state.currentDatasetsOrdering = payload;
  },

  [SET_SELECTED_PERMISSIONS]: (state, payload) => {
    if (payload.selected) {
      const index = state.selectedPermissions.findIndex(el => el.id === payload.item.id);
      if (index === -1) {
        state.selectedPermissions.push(payload.item);
      }
    } else {
      const index = state.selectedPermissions.findIndex(el => el.id === payload.item.id);
      if (index !== -1) {
        state.selectedPermissions.splice(index, 1);
      }
    }
  },
  [RESET_SELECTED_PERMISSIONS]: (state) => {
    state.selectedPermissions.splice(0);
  }
};
/**************** ACTIONS *****************/
const actions = {
  [PRE_SET_USERGROUPS_PERMISSIONS_FILTERS]: ({ state, commit, dispatch }, payload) => {
    if (payload.filter === 'level') {
      commit('pagination/SET_CURRENT_PAGE', 1, { root: true });
      commit('SET_USERGROUPS_PERMISSIONS_FILTERS', payload);
      dispatch('SEARCH_DATASET_USERGROUPS_PERMISSIONS', { text: state.searchPermissionsFilter });
    } else if (state.currentUsergroupsPermissionsFilters[payload.filter].findIndex(el => el === payload.value) === -1) {
      if (payload.filter === 'organisation_type_id') {
        commit('pagination/SET_CURRENT_PAGE', 1, { root: true });
        commit('SET_USERGROUPS_PERMISSIONS_FILTERS', payload);
        dispatch('SEARCH_DATASET_USERGROUPS_PERMISSIONS', { text: state.searchPermissionsFilter });
      } else {
        commit('pagination/SET_CURRENT_PAGE', 1, { root: true });
        commit('SET_USERGROUPS_PERMISSIONS_FILTERS', payload);
      }
    }
  },
  [PRE_REMOVE_USERGROUPS_PERMISSIONS_FILTERS]: ({ state, commit }, payload) => {
    const index = state.currentUsergroupsPermissionsFilters[payload.filter].findIndex(el => el === payload.value);
    if (index !== -1) {
      commit('REMOVE_USERGROUPS_PERMISSIONS_FILTERS', { index: index, filter: payload });
    }
  },
  [PRE_RESET_USERGROUPS_PERMISSIONS_FILTERS]: ({ state, commit }, payload) => {
    commit('pagination/SET_CURRENT_PAGE', 1, { root: true });
    commit('RESET_USERGROUPS_PERMISSIONS_FILTERS', payload);
  },
  

  [GET_DATASETS_COUNT]: async({ commit }) => {
    const datasetsCount = await datasetsAPI.getDatasetsCount();
    commit('SET_DATASETS_COUNT', datasetsCount);
  },
  [GET_DATASETS_LIST]: async ({ state, getters, commit, dispatch }, { direction, field, page }) => {
    let datasets;
    if (!page) {
      page = getters.tablePage;
    }
    // Save sorting params
    commit('SET_DATASETS_ORDERING', { direction, field });
    if (state.isDatasetsListSearched) {
      dispatch('SEARCH_DATASETS_LIST', { text: state.searchDatasetsFilter })
    } else if (field) {
      datasets = await datasetsAPI.orderDatasetsList(direction, field, page);
    } else {
      datasets = await datasetsAPI.getDatasetsList(page);
    }
    commit('SET_DATASETS_LIST', datasets);
  },

  [SEARCH_DATASETS_LIST]: async ({ state, commit, dispatch }, { text, ordering }) => {
    if (!ordering) {
      ordering = state.currentDatasetsOrdering;
    }
    if (text) {
      await dispatch('HANDLE_DATASETS_SEARCH_REQUEST', { text, ordering });
    } else {
      commit('SET_IS_DATASETSLIST_SEARCHED', {
        isSearched: false,
        text: null
      });
      await dispatch('GET_DATASETS_LIST', { ...ordering });
    }
  },

  [HANDLE_DATASETS_SEARCH_REQUEST]: async ({ state, rootState, getters, commit }, { text, ordering }) => {

    if (rootState.cancellableSearchRequest.length > 0) {
      const currentRequestCancelToken =
        rootState.cancellableSearchRequest[rootState.cancellableSearchRequest.length - 1];
      currentRequestCancelToken.cancel();
    }

    const cancelToken = axios.CancelToken.source();
    commit('SET_CANCELLABLE_SEARCH_REQUEST', cancelToken, { root: true });

    let url;
    if (ordering.field) {
      url = new URL(path.join(DATASET_API_PATH, `datasets/?ordering=${ordering.direction}${ordering.field}&page=${getters.tablePage}&search=${text}`), DOMAIN);
    } else {
      url = new URL(path.join(DATASET_API_PATH, `datasets/?page=${getters.tablePage}&search=${text}`), DOMAIN);
    }

    try {
      const response = await axios.get(
        url,
        {
          cancelToken: cancelToken.token,
          ...DEV_AUTH && { auth: AUTH }
        }
      );
      if (response.status === 200) {
        const datasets = response.data;
        if (datasets) {
          commit('SET_ERROR', null);
          commit('SET_DATASETS_LIST', datasets);
          commit('SET_IS_DATASETSLIST_SEARCHED', {
            isSearched: true,
            text: text
          });
        }
      }
    } catch(err) {
      commit('SET_ERROR', err);
    }
  },

  [GET_DATASET_DETAIL]: async ({ commit }, id) => {
    const dataset = await datasetsAPI.getDataset(id);
    commit('SET_CURRENT_DATASET', dataset);
  },
  DUPLICATE_DATASET: async ({ commit }, id) => {
    const dataset = await datasetsAPI.getDataset(id);
    commit('SET_DUPLICATED_DATASET', dataset);
  },
  [GET_DATASET_USERGROUPS_PERMISSIONS]: async ({ state, getters, commit }, {direction, field, id, page}) => {

    // Save sorting params
    commit('SET_USERGROUPS_PERMISSIONS_ORDERING', {direction, field});

    if (!page) {
      page = getters.tablePage;
    }
    if (!id) {
      id = state.currentUsergroupsPermissions[0] ?
        state.currentUsergroupsPermissions[0].dataset.toString() :
        state.currentDataset.id.toString();
    }
    let filters = JSON.parse(JSON.stringify(state.currentUsergroupsPermissionsFilters));
    if (state.searchPermissionsFilter) {
      filters.search = [state.searchPermissionsFilter];
    }
    if (field) {
      await datasetsAPI.orderDatasetPermissions(direction, field, id, filters, page)
      .then((permissions) => {
        if (permissions) {
          commit('SET_ERROR', null);
          commit('SET_CURRENT_USERGROUPS_PERMISSIONS', permissions);
        }
      })
      .catch((error) => {
        console.error(error);
        commit('SET_ERROR', error);
      });
    } else  {
      await datasetsAPI.getDatasetPermissions(id, filters, page)
      .then((permissions) => {
        if (permissions) {
          commit('SET_ERROR', null);
          commit('SET_CURRENT_USERGROUPS_PERMISSIONS', permissions);
        }
      })
      .catch((error) => {
        console.error(error);
        commit('SET_ERROR', error);
      });
    }
  },

  [SEARCH_DATASET_USERGROUPS_PERMISSIONS]: async ({ state, rootState, commit, dispatch }, {text, ordering, unfiltered = false}) => {
    if (!ordering) {
      ordering = state.currentUsergroupsPermissionsOrdering;
    }
    if (text) {
      await dispatch('HANDLE_PERMISSIONS_SEARCH_REQUEST', { text, ordering, unfiltered });
    } else {
      if (rootState.cancellableSearchRequest.length > 0) {
        const currentRequestCancelToken = rootState.cancellableSearchRequest[rootState.cancellableSearchRequest.length - 1];
        currentRequestCancelToken.cancel();
      }
      commit('SET_IS_PERMISSIONS_LIST_SEARCHED', {
        isSearched: false,
        text: null
      });
      await dispatch('GET_DATASET_USERGROUPS_PERMISSIONS', {...ordering});
    }
  },
  [HANDLE_PERMISSIONS_SEARCH_REQUEST]: async ({ state, rootState, getters, commit }, {text, ordering, unfiltered = false}) => {

    if (rootState.cancellableSearchRequest.length > 0) {
      const currentRequestCancelToken = rootState.cancellableSearchRequest[rootState.cancellableSearchRequest.length - 1];
      currentRequestCancelToken.cancel();
    }

    const cancelToken = axios.CancelToken.source();
    commit('SET_CANCELLABLE_SEARCH_REQUEST', cancelToken, { root: true });

    let url;
    if (ordering.field) {
      url = new URL(
        path.join(
          DATASET_API_PATH,
          `usergroup-permissions/?dataset_id=${state.currentDataset.id}&ordering=${ordering.direction}${ordering.field}&page=${getters.tablePage}&search=${text}`
        ),
        DOMAIN
      );
    } else {
      url = new URL(
        path.join(
          DATASET_API_PATH,
          `usergroup-permissions/?dataset_id=${state.currentDataset.id}&page=${getters.tablePage}&search=${text}`
        ),
        DOMAIN
      );
    }

    // Add filters to url
    const filters = state.currentUsergroupsPermissionsFilters;
    let filteredUrl;
    if (!unfiltered) {
      filteredUrl = url.href;
      for (const filter in filters) {
        if (filters[filter] && filter !== 'search') {
          filteredUrl = filteredUrl.concat('', `&${filter}=${filters[filter].join(',')}`);
        }
      }
    }

    try {
      const response = await axios.get(
        filteredUrl ? filteredUrl : url,
        {
          cancelToken: cancelToken.token,
          ...DEV_AUTH && { auth: AUTH }
        }
      );
      if (response.status === 200) {
        const usergroupsPermissions = response.data;
        if (usergroupsPermissions) {
          commit('SET_ERROR', null);
          commit('SET_CURRENT_USERGROUPS_PERMISSIONS', usergroupsPermissions);
          commit('SET_IS_PERMISSIONS_LIST_SEARCHED', {
            isSearched: true,
            text: text
          });
        }
      }
    } catch(err) {
      console.error(err);
    }
  },

  [SET_DATASET_USERGROUPS_PERMISSIONS]: async ({ commit }, {level, data}) => {
    await datasetsAPI.setDatasetPermissions(level, data)
    .then((resp) => {
      if (resp) {
        commit('SET_ERROR', null);
        ErrorService.onSuccess(resp, 'Permission mise à jour.');
      }
    })
    .catch((error) => {
      console.error(error);
      commit('SET_ERROR', error);
    });
  },
  [GET_DATASETS_CATEGORIES]: async ({ commit }) => {
    const categories = await datasetsAPI.getDatasetsCategories();
    commit('SET_DATASETS_CATEGORIES', categories);
  },
  [GET_DATASETS_GRANULARITIES]: async ({ commit }) => {
    const granularities = await datasetsAPI.getDatasetsGranularities();
    commit('SET_DATASETS_GRANULARITIES', granularities);
  },
  [GET_DATASETS_LICENCES]: async ({ commit }) => {
    const licences = await datasetsAPI.getDatasetsLicences();
    commit('SET_DATASETS_LICENCES', licences);
  },
  [GET_DATASETS_TOPICS]: async ({ commit }) => {
    const topics = await datasetsAPI.getDatasetsTopics();
    commit('SET_DATASETS_TOPICS', topics);
  },
  [GET_DATASETS_KINDS]: async ({ commit }) => {
    const kinds = await datasetsAPI.getDatasetsKinds();
    commit('SET_DATASETS_KINDS', kinds);
  },
  [GET_DATASETS_RESTRICTIONS]: async ({ commit }) => {
    const kinds = await datasetsAPI.getDatasetsRestrictions();
    commit('SET_DATASETS_RESTRICTIONS', kinds);
  },
  [GET_DATASETS_UPDATE_FREQUENCIES]: async ({ commit }) => {
    const kinds = await datasetsAPI.getDatasetsUpdateFrequencies();
    commit('SET_DATASETS_UPDATE_FREQUENCIES', kinds);
  },
  [CREATE_DATASET]: async ({ commit }, data) => {
    try {
      const createdDataset = await datasetsAPI.createDataset(data);
      if (createdDataset && data.thumbnail) {
        const datasetThumbnail = await datasetsAPI.setDatasetThumbnail(createdDataset.id, data.thumbnail);
        if (datasetThumbnail) {
          commit('SET_ERROR', null);
          ErrorService.onSuccess(createdDataset, `Le jeu de données <b>${createdDataset.display_name}</b> a été créé avec succès.`);
          commit('SET_LAST_CREATED_DATASET', createdDataset);
        }
      } else if (createdDataset) {
        commit('SET_ERROR', null);
        ErrorService.onSuccess(createdDataset, `Le jeu de données <b>${createdDataset.display_name}</b> a été créé avec succès.`);
        commit('SET_LAST_CREATED_DATASET', createdDataset);
      }
    } catch (error) {
      console.error(error);
      ErrorService.onError(error);
      commit('SET_ERROR', error);
    }
  },
  [SET_DATASET_THUMBNAIL]: async ({ commit }, { id, data }) => {
    await datasetsAPI.setDatasetThumbnail(id, data)
    .then((resp) => {
      if (resp) {
        commit('SET_ERROR', null);
      }
    })
    .catch((error) => {
      commit('SET_ERROR', error);
    });
  },
  [UPDATE_DATASET]: async ({ commit }, { id, data }) => {
    await datasetsAPI.updateDataset(id, data).then((resp) => {
      if (resp) {
        commit('SET_ERROR', null);
        ErrorService.onSuccess(resp, 'Le jeu de données a été modifié avec succès.');
      }
    }).catch((error) => {
      commit('SET_ERROR', error);
    });
  },
  [PATCH_DATASET]: async ({ commit }, { id, data }) => {
    await datasetsAPI.patchDataset(id, data).then((resp) => {
      if (resp) {
        commit('SET_ERROR', null);
        ErrorService.onSuccess(resp, 'Le jeu de données a été modifié avec succès.');
      }
    }).catch((error) => {
      commit('SET_ERROR', error);
    });
  },
  [DELETE_DATASET]: async ({ commit, dispatch, rootState }, dataset) => {
    try {
      // Get datasets resources list before it's deleted
      commit('resources/SET_RESOURCES_FILTERS', {
        filter: 'dataset_id',
        value: dataset.id
      }, { root: true });
      await dispatch('resources/GET_RESOURCES_LIST', null, { root: true });
      const datasetResources = rootState.resources.resourcesList;

      // Delete dataset first
      const deletedDataset = await datasetsAPI.deleteDataset(dataset.id);
      if (deletedDataset) {
        commit('SET_ERROR', null);

        // Delete dataset's resources
        // if (datasetResources.length > 0) {
        //   const deletedResources = await Promise.all(
        //     datasetResources.map(async (resource) => {
        //       dispatch('resources/DELETE_RESOURCE', resource, { root: true });
        //     })
        //   );
        //   if (deletedResources) {
        //     ErrorService.onSuccess(true, `Le jeu de données <b>${dataset['Nom']}</b> a été supprimé avec succès.`);
        //   }
        // } else {
        //   ErrorService.onSuccess(true, `Le jeu de données <b>${dataset['Nom']}</b> a été supprimé avec succès.`);
        // }
      }
    } catch (err) {
      commit('SET_ERROR', err);
      ErrorService.onError(null, 'Une erreur est survenue.');
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
