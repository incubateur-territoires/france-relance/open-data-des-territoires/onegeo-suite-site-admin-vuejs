import resourcesAPI from '@/api/resourcesAPI.js';

import { ErrorService } from '@/services/error-service.js';

// MUTATIONS
export const SET_RESOURCES_LIST = 'SET_RESOURCES_LIST';
export const SET_CURRENT_RESOURCE = 'SET_CURRENT_RESOURCE';
export const SET_RESOURCE_DATA_FORMATS = 'SET_RESOURCE_DATA_FORMATS';
export const SET_RESOURCE_KINDS = 'SET_RESOURCE_KINDS';
export const SET_RESOURCE_INSPECTION_DATA = 'SET_RESOURCE_INSPECTION_DATA';
export const SET_CURRENT_RESOURCE_FILE_ID = 'SET_CURRENT_RESOURCE_FILE_ID';
export const SET_RESOURCES_FILTERS = 'SET_RESOURCES_FILTERS';
export const REMOVE_RESOURCES_FILTERS = 'REMOVE_RESOURCES_FILTERS';
export const SET_LAST_CREATED_RESOURCE = 'SET_LAST_CREATED_RESOURCE';
export const SET_ERROR = 'SET_ERROR';

// ACTIONS
export const GET_RESOURCES_LIST = 'GET_RESOURCES_LIST';
export const GET_RESOURCE_DETAIL = 'GET_RESOURCE_DETAIL';
export const GET_RESOURCE_DATA_FORMATS = 'GET_RESOURCE_DATA_FORMATS';
export const GET_RESOURCE_KINDS = 'GET_RESOURCE_KINDS';
export const INSPECT_FILE_UPLOAD = 'INSPECT_FILE_UPLOAD';
export const INSPECT_FTP = 'INSPECT_FTP';
export const INSPECT_HREF = 'INSPECT_HREF';
export const DELETE_RESOURCE_FILE = 'DELETE_RESOURCE_FILE';
export const CREATE_FILE_UPLOAD_RESOURCE = 'CREATE_FILE_UPLOAD_RESOURCE';
export const CREATE_FTP_RESOURCE = 'CREATE_FTP_RESOURCE';
export const CREATE_HREF_RESOURCE = 'CREATE_HREF_RESOURCE';
export const CREATE_FILE_UPLOAD_ANNEXE = 'CREATE_FILE_UPLOAD_ANNEXE';
export const CREATE_FTP_ANNEXE = 'CREATE_FTP_ANNEXE';
export const CREATE_HREF_ANNEXE = 'CREATE_HREF_ANNEXE';
export const APPEND_ANNEXE_TO_DATASET = 'APPEND_ANNEXE_TO_DATASET';
export const UPDATE_RESOURCE_FILE = 'UPDATE_RESOURCE_FILE';
export const UPDATE_FTP_RESOURCE = 'UPDATE_FTP_RESOURCE';
export const UPDATE_HREF_RESOURCE = 'UPDATE_HREF_RESOURCE';
export const UPDATE_HREF_ANNEXE = 'UPDATE_HREF_ANNEXE';
export const UPDATE_FILE_UPLOAD_RESOURCE = 'UPDATE_FILE_UPLOAD_RESOURCE';
export const DELETE_RESOURCE = 'DELETE_RESOURCE';
export const DELETE_RESOURCE_FROM_DATASET = 'DELETE_RESOURCE_FROM_DATASET';
export const PATCH_RESOURCE = 'PATCH_RESOURCE';

/**************** STATE *******************/
const state = {
  resourcesCount: 0,
  resourcesList: [],
  currentResource: {},
  lastCreatedResource: null,
  resourceDataFormats: [],
  resourceKinds: [],
  resourcesError: null,
  resourceInspectionData: null,
  currentResourceFileId: null,
  currentResourcesFilters: {
    resource_type_id: [],
    dataset_id: []
  }
};

/**************** GETTERS *****************/
const getters = {
  tablePage: (state, getters, rootState) => {
    return rootState.pagination.currentPage;
  }
};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_RESOURCES_LIST]: (state, payload) => {
    if (payload.count) {
      state.resourcesCount = payload.count;
    }
    if (payload.results) {
      state.resourcesList = payload.results;
    }
  },
  [SET_CURRENT_RESOURCE]: (state, payload) => {
    if (payload) {
      state.currentResource = payload;
    }
  },

  [SET_RESOURCE_DATA_FORMATS]: (state, payload) => {
    if (payload) {
      state.resourceDataFormats = payload;
    }
  },

  [SET_RESOURCE_KINDS]: (state, payload) => {
    if (payload) {
      state.resourceKinds = payload;
    }
  },

  [SET_CURRENT_RESOURCE_FILE_ID]: (state, payload) => {
    state.currentResourceFileId = payload;
  },

  [SET_RESOURCE_INSPECTION_DATA]: (state, payload) => {
    if (payload && payload.data_info) {
      state.resourceInspectionData = payload.data_info;
    } else if (payload === null) {
      state.resourceInspectionData = payload;
    }
  },

  [SET_RESOURCES_FILTERS]: (state, payload) => {
    if (state.currentResourcesFilters[payload.filter].findIndex(el => el === payload.value) === -1) {
      if (payload.filter === 'dataset_id') {
        state.currentResourcesFilters[payload.filter] = [payload.value];
      } else {
        state.currentResourcesFilters[payload.filter].push(payload.value);
      }
    }
  },

  [REMOVE_RESOURCES_FILTERS]: (state, payload) => {
    const index = state.currentResourcesFilters[payload.filter].findIndex(el => el === payload.value);
    if (index !== -1) {
      state.currentResourcesFilters[payload.filter].splice(index, 1);
    }
  },

  [SET_LAST_CREATED_RESOURCE]: (state, payload) => {
    state.lastCreatedResource = payload;
  },

  [SET_ERROR]: (state, error) => {
    if (error) {
      ErrorService.onError(error);
    }
    state.resourcesError = error;
  }
};
/**************** ACTIONS *****************/
const actions = {
  [GET_RESOURCES_LIST]: async ({ state, commit }) => {
    const filters = state.currentResourcesFilters;
    const resources = await resourcesAPI.getResourcesList(filters);
    commit('SET_RESOURCES_LIST', resources);
  },

  [GET_RESOURCE_DETAIL]: async ({ commit }, id) => {
    const resource = await resourcesAPI.getResource(id);
    commit('SET_CURRENT_RESOURCE', resource);
  },

  [PATCH_RESOURCE]: async ({ commit }, {id, data, datasetId}) => {
    const resource = await resourcesAPI.patchResource(id, data, datasetId);
  },

  [GET_RESOURCE_DATA_FORMATS]: async ({ commit }) => {
    const formats = await resourcesAPI.getResourceDataFormats();
    commit('SET_RESOURCE_DATA_FORMATS', formats);
  },

  [GET_RESOURCE_KINDS]: async ({ commit }) => {
    const kinds = await resourcesAPI.getResourceKinds();
    commit('SET_RESOURCE_KINDS', kinds);
  },

  [UPDATE_RESOURCE_FILE]: async ({ commit }, { location, file, format }) => {
    try {
      const resp1 = await resourcesAPI.updateFileUploadFormat(location, format.codename);
      const resp2 = await resourcesAPI.updateFileUploadFile(location, file);
      commit('SET_CURRENT_RESOURCE_FILE_ID', resp1.id);
      const resp3 = await resourcesAPI.createFileUploadInspect(resp1.id);
      if (resp3.data_info) {
        commit('SET_RESOURCE_INSPECTION_DATA', resp2);
        commit('SET_ERROR', null);
      } else {
        throw new Error('L\'inspection du fichier a échoué. Vérifiez que votre fichier est correctement formaté et réessayez.');
      }
    } catch(err) {
      commit('SET_ERROR', err);
    }
  },

  [DELETE_RESOURCE_FILE]: async ({ commit }, id) => {
    await resourcesAPI.deleteFileUploadFile(id)
    .then(() => {
      commit('SET_RESOURCE_INSPECTION_DATA', null);
      commit('SET_CURRENT_RESOURCE_FILE_ID', null);
      commit('SET_ERROR', null);
    })
    .catch((err) => {
      commit('SET_ERROR', err);
    });
  },

  [INSPECT_FILE_UPLOAD]: async ({ commit }, data) => {
    try {
      const resp1 = await resourcesAPI.createFileUploadFormat(data.format.codename);
      const resp2 = await resourcesAPI.createFileUploadFile(resp1.id, data.file);
      commit('SET_CURRENT_RESOURCE_FILE_ID', resp2.id);
      const resp3 = await resourcesAPI.createFileUploadInspect(resp2.id);
      if (resp3.data_info && resp3.data_info.is_valid) {
        commit('SET_RESOURCE_INSPECTION_DATA', resp3);
        commit('SET_ERROR', null);
      } else {
        throw new Error('L\'inspection du fichier a échoué. Vérifiez que votre fichier est correctement formaté et réessayez.');
      }
    } catch(err) {
      ErrorService.onError(null, err);
      commit('SET_ERROR', err);
      throw new Error(err);
    }
  },

  [CREATE_FILE_UPLOAD_RESOURCE]: async ({ state, commit }, { data, async }) => {
    try {
      const createdResource = await resourcesAPI.createFileUploadResource(state.currentResourceFileId, data, async)
      if (createdResource) {
        commit('SET_LAST_CREATED_RESOURCE', createdResource);
        if (!createdResource.data_info.error) {
          ErrorService.onSuccess(createdResource, `La création de la ressource <b>${createdResource.file.filename}</b> a été lancée avec succès.`);
        }
      }
    } catch (err) {
      commit('SET_ERROR', err);
    }
  },

  [INSPECT_FTP]: async ({ commit }, data) => {
    try {
      const resp1 = await resourcesAPI.createFTPFormat(data);
      commit('SET_CURRENT_RESOURCE_FILE_ID', resp1.id);
      const resp2 = await resourcesAPI.createFTPInspect(resp1.id);
      if (resp2.data_info && resp2.data_info.is_valid) {
        commit('SET_RESOURCE_INSPECTION_DATA', resp2);
        commit('SET_ERROR', null);
      } else {
        if (resp2.data_info && resp2.data_info.error !== null) {
          throw new Error(resp2.data_info.error);
        } else {
          throw new Error('L\'inspection du(des) fichier(s) a échoué. Vérifiez que votre(vos) fichier(s) est(sont) correctement formaté(s) et réessayez.');
        }
      }
    } catch(err) {
      ErrorService.onError(null, err);
      commit('SET_ERROR', err);
      throw new Error(err);
    }
  },

  [CREATE_FTP_RESOURCE]: async ({ state, commit }, { data, async }) => {
    try {
      const createdResource = await resourcesAPI.createFTPResource(state.currentResourceFileId, data)
      if (createdResource) {
        commit('SET_LAST_CREATED_RESOURCE', createdResource);
        ErrorService.onSuccess(createdResource, `La création de la ressource <b>${createdResource.file.filename}</b> a été lancée avec succès.`);
      }
    } catch (err) {
      commit('SET_ERROR', err);
    }
  },

  [INSPECT_HREF]: async ({ commit }, data) => {
    try {
      const resp1 = await resourcesAPI.createHrefFormat(data);
      commit('SET_CURRENT_RESOURCE_FILE_ID', resp1.id);
      const resp2 = await resourcesAPI.createHrefInspect(resp1.id);
      if (resp2.data_info) {
        commit('SET_RESOURCE_INSPECTION_DATA', resp2);
        commit('SET_ERROR', null);
      } else {
        throw new Error('L\'inspection de l\'url a échoué. Vérifiez que votre url est correctement formatée et réessayez.');
      }
    } catch(err) {
      ErrorService.onError(null, err);
      commit('SET_ERROR', err);
      throw new Error(err);
    }
  },

  [CREATE_HREF_RESOURCE]: async ({ state, commit }, { data, async }) => {
    try {
      const createdResource = await resourcesAPI.createHrefResource(state.currentResourceFileId, data);
      if (createdResource) {
        commit('SET_LAST_CREATED_RESOURCE', createdResource);
        ErrorService.onSuccess(createdResource, `La création de la ressource <b>${createdResource.file.filename}</b> a été lancée avec succès.`);
      }
    } catch (err) {
      commit('SET_ERROR', err);
    }
  },

  [UPDATE_FILE_UPLOAD_RESOURCE]: async ({ state, commit }, { type, data, datasetId, resourceId, async }) => {
    try {
      if (type === 'resource') {
        const updatedResource = await resourcesAPI.updateFileUploadResource(state.currentResourceFileId, data, datasetId, async);
        if (updatedResource) {
          commit('SET_LAST_CREATED_RESOURCE', updatedResource);
          ErrorService.onSuccess(updatedResource, `La mise à jour de la ressource <b>${updatedResource.file.filename}</b> a été lancée avec succès.`);
        }
      } else {
        const updatedResource = await resourcesAPI.updateFileUploadAnnexe(datasetId, resourceId);
        if (updatedResource) {
          commit('SET_LAST_CREATED_RESOURCE', updatedResource);
          ErrorService.onSuccess(updatedResource, `La mise à jour de l'annexe <b>${updatedResource.file.filename}</b> a été lancée avec succès.`);
        }
      }
    } catch (err) {
      console.error(err);
      commit('SET_ERROR', err);
    }
  },

  [UPDATE_FTP_RESOURCE]: async ({ commit }, { type, data, datasetId, resourceId }) => {
    try {
      if (type === 'resource') {
        const updatedResource = await resourcesAPI.updateFTPResource(data, datasetId);
        if (updatedResource) {
          commit('SET_LAST_CREATED_RESOURCE', updatedResource);
          ErrorService.onSuccess(updatedResource, `La mise à jour de la ressource <b>${updatedResource.file.filename}</b> a été lancée avec succès.`);
        }
      } else {
        const updatedResource = await resourcesAPI.updateFileUploadAnnexe(datasetId, resourceId);
        if (updatedResource) {
          commit('SET_LAST_CREATED_RESOURCE', updatedResource);
          ErrorService.onSuccess(updatedResource, `La mise à jour de l'annexe <b>${updatedResource.file.filename}</b> a été lancée avec succès.`);
        }
      }
    } catch (err) {
      console.error(err);
      commit('SET_ERROR', err);
    }
  },

  [UPDATE_HREF_RESOURCE]: async ({ commit }, { data, async }) => {
    try {
      const updatedResource = await resourcesAPI.updateHrefResource(data, async);
      if (updatedResource) {
        commit('SET_LAST_CREATED_RESOURCE', updatedResource);
        ErrorService.onSuccess(updatedResource, `La mise à jour de la ressource <b>${updatedResource.resource.display_name ? updatedResource.resource.display_name : ''}</b> a été lancée avec succès.`);
      }
    } catch (err) {
      console.error(err);
      commit('SET_ERROR', err);
    }
  },
  [UPDATE_HREF_ANNEXE]: async ({ commit }, { data, datasetId, resourceId, async }) => {
    try {
      const updatedResource = await resourcesAPI.updateHrefAnnexe(data, datasetId, resourceId, async);
      if (updatedResource) {
        commit('SET_LAST_CREATED_RESOURCE', updatedResource);
        ErrorService.onSuccess(updatedResource, `La mise à jour de la ressource <b>${updatedResource.resource.display_name ? updatedResource.resource.display_name : ''}</b> a été lancée avec succès.`);
      }
    } catch (err) {
      console.error(err);
      commit('SET_ERROR', err);
    }
  },

  [CREATE_FILE_UPLOAD_ANNEXE]: async ({ dispatch }, data) => {
    try {
      const resp1 = await resourcesAPI.createFileUploadFormat(data.format.codename);
      const resp2 = await resourcesAPI.createFileUploadFile(resp1.id, data.file);
      const kwargs = {
        kwargs: {
          resource__display_name: data.display_name,
          resource__description: data.description,
          resource__kind: data.kind,
          usergroup_pk: data.usergroup_pk
        }
      };
      const resp3 = await resourcesAPI.createFileUploadResourceAnnexe(resp2.id, kwargs);
      ErrorService.onSuccess(resp3, 'La ressource annexe a été créé avec succès.');
      dispatch('APPEND_ANNEXE_TO_DATASET', {
        resourceId: resp3.resource.id,
        dataset: data.dataset
      });
    } catch (err) {
      console.error(err);
      commit('SET_ERROR', err);
    }
  },

  [CREATE_FTP_ANNEXE]: async ({ dispatch }, data) => {
    try {
      const resp1 = await resourcesAPI.createFTPFormat({
        data_format: data.data_format,
        href: data.href
      });
      const kwargs = {
        kwargs: {
          resource__display_name: data.display_name,
          resource__description: data.description,
          resource__kind: data.kind,
          usergroup_pk: data.usergroup_pk
        }
      };
      const resp2 = await resourcesAPI.createFTPResourceAnnexe(resp1.id, kwargs);
      ErrorService.onSuccess(resp2, 'La ressource annexe a été créé avec succès.');
      dispatch('APPEND_ANNEXE_TO_DATASET', {
        resourceId: resp2.resource.id,
        dataset: data.dataset
      });
    } catch (err) {
      console.error(err);
      commit('SET_ERROR', err);
    }
  },

  [CREATE_HREF_ANNEXE]: async ({ dispatch }, data) => {
    try {
      const resp1 = await resourcesAPI.createHrefFormat(data);
      const kwargs = {
        kwargs: {
          resource__display_name: data.display_name,
          resource__description: data.description,
          resource__kind: data.kind,
          usergroup_pk: data.usergroup_pk
        }
      };
      const resp2 = await resourcesAPI.createHrefResourceAnnexe(resp1.id, kwargs);
      ErrorService.onSuccess(resp2, 'La ressource annexe a été créé avec succès.');
      dispatch('APPEND_ANNEXE_TO_DATASET', {
        resourceId: resp2.resource.id,
        dataset: data.dataset
      });
    } catch (err) {
      console.error(err);
      commit('SET_ERROR', err);
    }
  },

  [APPEND_ANNEXE_TO_DATASET]: ({ commit }, data) => {
    resourcesAPI.appendAnnexeResourceToDataset(
      {
        type: 2,
        resource: data.resourceId,
        dataset: data.dataset
      }
    )
      .catch((err) => {
        console.error(err);
        commit('SET_ERROR', err);
      });
  },

  [DELETE_RESOURCE_FROM_DATASET]: async ({ commit }, { resourceToDatasetId, resource }) => {
    try {
      await resourcesAPI.deleteResourceToDataset(resourceToDatasetId);
      // await resourcesAPI.deleteResource(resource.id);
      commit('SET_ERROR', null);
      ErrorService.onSuccess(true, `La ressource <b>${resource.display_name}</b> a été supprimée avec succès.`);
    } catch (error) {
      commit('SET_ERROR', error);
    }
  },

  [DELETE_RESOURCE]: async ({ commit }, resource) => {
    try {
      await resourcesAPI.deleteResource(resource.id);
      commit('SET_ERROR', null);
      ErrorService.onSuccess(true, `La ressource <b>${resource.display_name}</b> a été supprimée avec succès.`);
    } catch (error) {
      commit('SET_ERROR', error);
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
